from django.shortcuts import render
from .models import AluminiumCatalog
from myfunction import page
# Create your views here.

def aluminium(request):
    context = {}
    return render(request, 'aluminium/aluminium.html', context)

def aluminium_door(request):
    aluminium_door_catalog = AluminiumCatalog.objects.filter(type__type='Aluminium Door').order_by('my_id')
    context = {
        'aluminium_door':page(aluminium_door_catalog),
        'aluminium_door_page':aluminium_door_catalog,
    }
    return render(request, 'aluminium/aluminium_door.html', context)

def aluminium_cabinet(request):
    aluminium_cabinet_catalog = AluminiumCatalog.objects.filter(type__type='Aluminium Cabinet').order_by('my_id')
    context = {
        'aluminium_cabinet':page(aluminium_cabinet_catalog),
        'aluminium_cabinet_page':aluminium_cabinet_catalog,
    }
    return render(request, 'aluminium/aluminium_cabinet.html', context)


def aluminium_shower_screen(request):
    aluminium_shower_screen_catalog = AluminiumCatalog.objects.filter(type__type="Shower Screen").order_by("my_id")
    context = {
        "aluminium_shower_screen":page(aluminium_shower_screen_catalog), #name of this must be same as name for def contextElement!!!
        "aluminium_shower_screen_page":aluminium_shower_screen_catalog,
    }
    return render(request, 'aluminium/aluminium_shower_screen.html', context)