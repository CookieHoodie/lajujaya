from django.contrib import admin
from .models import AluminiumType, AluminiumCatalog

# Register your models here.

class AluminiumCatalogAdmin(admin.ModelAdmin):
    list_display = ('type','my_id','name', 'image', 'description')
    list_filter = ['type','name']

admin.site.register(AluminiumType)
admin.site.register(AluminiumCatalog, AluminiumCatalogAdmin)
