from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.aluminium, name='aluminium'),
    url(r'^aluminium-door$', views.aluminium_door, name="aluminium_door"),
    url(r'^aluminium-cabinet$', views.aluminium_cabinet, name="aluminium_cabinet"),
    url(r'^shower-screen', views.aluminium_shower_screen, name='shower_screen'),
]