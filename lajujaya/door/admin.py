from django.contrib import admin
from .models import DoorType, DoorCatalog

# Register your models here.

class DoorCatalogAdmin(admin.ModelAdmin):
    list_display = ('type','my_id','name', 'image', 'description')
    list_filter = ['type','name']

admin.site.register(DoorType)
admin.site.register(DoorCatalog, DoorCatalogAdmin)
