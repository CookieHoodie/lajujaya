from django.shortcuts import render
from .models import DoorCatalog
# my own function
from myfunction import page
# Create your views here.

def door(request):
    context = {}
    return render(request, 'door/door.html', context)

def solid_wooden_door(request):
    solid_wooden_door_catalog = DoorCatalog.objects.filter(type__type="Solid Wooden Door").order_by("my_id")
    context = {
        "solid_wooden_door":page(solid_wooden_door_catalog), #name of this must be same as name for def contextElement!!!
        "solid_wooden_door_page":solid_wooden_door_catalog,
    }
    # contextElement("solid_wooden_door",context, solid_wooden_door_catalog) #name - "door" == name used in template ---- (if the list is more than $size in def page()
    return render(request, 'door/solid_wooden_door.html', context)

def firedoor(request):
    firedoor_catalog =  DoorCatalog.objects.filter(type__type="Fire Door").order_by("my_id")
    context = {
        "firedoor":page(firedoor_catalog),
        "firedoor_page":firedoor_catalog,
    }
    return render(request, 'door/firedoor.html', context)

def MPC_design_door(request):
    MPC_design_door_catalog = DoorCatalog.objects.filter(type__type="MPC Design Door").order_by("my_id")
    context = {
        "MPC_design_door":page(MPC_design_door_catalog),
        "MPC_design_door_page":MPC_design_door_catalog,
    }
    return render(request, 'door/MPC_design_door.html', context)