from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.door, name='door'),
    url(r'^solid-wooden-door/$', views.solid_wooden_door, name="solid_wooden_door"),
    url(r'^firedoor/$', views.firedoor, name="firedoor"),
    url(r'^MPC-design-door/$', views.MPC_design_door, name="MPC_design_door"),
]