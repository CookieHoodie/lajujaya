from .base import *

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
ALLOWED_HOSTS = ['.lajujayatrd.com']
DEBUG = False
TEMPLATE_DEBUG = False