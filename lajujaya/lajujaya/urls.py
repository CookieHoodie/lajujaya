"""lajujaya URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^secret-admin-configure-page/', include(admin.site.urls)),
    url(r'^$', views.index, name="home"),
    url(r'^door/', include('door.urls', namespace="door")),
    url(r'^doorlock/', include('doorlock.urls', namespace='doorlock')),
    url(r'^enquiry/', include('enquiry.urls', namespace='enquiry')),
    url(r'^aluminium/', include('aluminium.urls', namespace='aluminium')),
    url(r'^kitchen-and-bathroom-accessories/', include('kitchen_bathroom_accessories.urls', namespace='kitchen_bathroom_accessories')),
    url(r'^others/', include('others.urls', namespace='others')),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^services$', views.services, name='services'),
    url(r'^FAQ$', views.faq, name='faq'),
    url(r'^(?P<filename>(robots.txt)|(humans.txt))$',
        views.home_files, name='home-files'),
]

if settings.DEBUG:
        urlpatterns += (url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.MEDIA_ROOT,
            }),
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.STATIC_ROOT,
            }))

