from django.shortcuts import render
from enquiry.forms import Contact_usForm
from others.models import Carosel

def index(request):
    cover_header = "Laju Jaya Trading"
    cover_description = "Quality Products with Prompt"
    carosel = Carosel.objects.order_by('page')

    context = {
        "cover_header":cover_header,
        "cover_description":cover_description,
        "carosel":carosel,
    }

    return render(request, "index.html", context)

def services(request):
    context = {}
    return render(request, 'services.html',context)

def contact(request):
    form = Contact_usForm()
    context = {'form':form}
    return render(request, 'contact.html',context)

def faq(request):
    context = {}
    return render(request, 'faq.html',context)

def home_files(request, filename):
    return render(request, filename, {}, content_type="text/plain")