# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('enquiry', '0002_auto_20151208_1051'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact_us',
            name='date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2015, 12, 8, 8, 39, 37, 387016, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
