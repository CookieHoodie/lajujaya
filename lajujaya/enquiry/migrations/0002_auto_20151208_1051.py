# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('enquiry', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact_us',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(verbose_name='Your Email', max_length=254)),
                ('phone_number', models.CharField(max_length=15, help_text='Please use the following format: <em>+6...</em>.', validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+6...'", regex='^\\+?1?\\d{9,15}$')])),
                ('content', models.TextField(max_length=500)),
            ],
        ),
        migrations.AlterField(
            model_name='enquiry',
            name='email',
            field=models.EmailField(verbose_name='Your Email', max_length=254),
        ),
        migrations.AlterField(
            model_name='enquiry',
            name='phone_number',
            field=models.CharField(max_length=15, help_text='Please use the following format: <em>+6...</em>.', validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+6...'", regex='^\\+?1?\\d{9,15}$')]),
        ),
    ]
