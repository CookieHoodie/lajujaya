from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
from .forms import EnquiryForm, Contact_usForm
# Create your views here.

def enquiry(request): #views that process contact form

    form = EnquiryForm(request.POST or None)
    context = {
        "form":form,
    }
    if form.is_valid():
        instance = form.save(commit=False)

        name = form.cleaned_data.get('name')
        products = form.cleaned_data.get('product')
        message = form.cleaned_data.get('enquiry')
        if form.cleaned_data.get('address'):
            address = form.cleaned_data.get('address')
        else :
            address = 'None'
        customer_email = form.cleaned_data.get('email')
        phone_number = form.cleaned_data.get('phone_number')
        subject = 'Customer Enquiry'
        enquiry_message = '''
            Name: %s
            Phone_number: %s
            Address: %s
            Customer Email: %s   *reply to this email address
            Product Interested: %s
            Enquiry: %s
        ''' %(name, phone_number, address, customer_email, products, message)
        our_email = settings.EMAIL_HOST_USER

        send_mail(subject, enquiry_message, our_email, ["lajujayatrd@gmail.com"], fail_silently=False)
        instance.save()
        context['cls'] = 'alert alert-success'
        context['thanks'] = 'Email sent! We will reply you as soon as possible!'
        form = EnquiryForm()

    return render(request, 'enquiry/enquiry.html',context)

def contact_us(request): #views that process contact form

    form = Contact_usForm(request.POST or None)
    context = {
        "form":form,
    }
    if form.is_valid():
        instance = form.save(commit=False)

        name = form.cleaned_data.get('name')
        message = form.cleaned_data.get('content')
        customer_email = form.cleaned_data.get('email')
        phone_number = form.cleaned_data.get('phone_number')
        subject = 'Customer Enquiry via Contact Us'
        enquiry_message = '''
            Name: %s
            Phone_number: %s
            Customer Email: %s   *reply to this email address
            Content: %s
        ''' %(name, phone_number, customer_email, message)
        our_email = settings.EMAIL_HOST_USER

        send_mail(subject, enquiry_message, our_email, ["lajujayatrd@gmail.com"], fail_silently=False)
        instance.save()
        context['cls'] = 'alert alert-success'
        context['thanks'] = 'Email sent! We will reply you as soon as possible!'
        form = EnquiryForm()

    return render(request, 'enquiry/contact_us.html',context)