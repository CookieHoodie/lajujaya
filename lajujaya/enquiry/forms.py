from django import forms
from .models import Enquiry, Contact_us

class EnquiryForm(forms.ModelForm):
    class Meta:
        model = Enquiry
        fields = ['name','email','phone_number','product','address','enquiry']

class Contact_usForm(forms.ModelForm):
    class Meta:
        model = Contact_us
        fields = ['name','email','phone_number','content']