from django.db import models
from django.core.validators import RegexValidator
# Create your models here.

class Enquiry(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(verbose_name='Your Email')
    product = models.CharField(max_length=100, help_text="Please enter the name of the product you interest in.")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+6...'")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, help_text="Please use the following format: <em>+6...</em>.") # validators should be a list
    address = models.CharField(max_length=200, blank=True)
    enquiry = models.TextField(max_length=500)
    date = models.DateTimeField(auto_now_add=True, auto_now=False)

class Contact_us(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(verbose_name='Your Email')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+6...'")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, help_text="Please use the following format: <em>+6...</em>.") # validators should be a list
    content = models.TextField(max_length=500)
    date = models.DateTimeField(auto_now_add=True, auto_now=False)