from django.contrib import admin
from .models import Enquiry, Contact_us
# Register your models here.

class EnquiryAdmin(admin.ModelAdmin):
    list_display = ('name','email','product','phone_number','date')
    list_filter = ['date']

class Contact_usAdmin(admin.ModelAdmin):
    list_display = ('name','email','phone_number', 'date')
    list_filter = ['date']

admin.site.register(Enquiry, EnquiryAdmin)
admin.site.register(Contact_us, Contact_usAdmin)