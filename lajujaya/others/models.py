from django.db import models
import os
# Create your models here.

def path_and_rename(instance, filename):
    upload_to = 'others/'
    ext = filename.split('.')[-1]
    ori_name = filename.split('.')[0]
    # get filename
    if instance.name:
        filename = '{}.{}'.format(instance.name, ext)
    else:
        pass
    # return the whole path to the file
    return os.path.join(upload_to, filename)

class OthersType(models.Model):
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.type

class OthersCatalog(models.Model):
    type = models.ForeignKey(OthersType)
    my_id = models.IntegerField(null=True)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=path_and_rename)
    description = models.TextField(max_length=500, blank=True)

    def __str__(self):
        return self.name

class Carosel(models.Model):
    page = models.IntegerField()
    image = models.ImageField(upload_to=path_and_rename)
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name