from django.shortcuts import render
from .models import OthersCatalog
from myfunction import page
# Create your views here.

def others(request):
    context = {}
    return render(request, 'others/others.html', context)

def others_staircase(request):
    others_staircase_catalog = OthersCatalog.objects.filter(type__type="Staircase").order_by("my_id")
    context = {
        "others_staircase":page(others_staircase_catalog), #name of this must be same as name for def contextElement!!!
        "others_staircase_page":others_staircase_catalog,
    }
    return render(request, 'others/others_staircase.html', context)

def others_wooden_window(request):
    others_wooden_window_catalog = OthersCatalog.objects.filter(type__type="Wooden Window").order_by("my_id")
    context = {
        "others_wooden_window":page(others_wooden_window_catalog), #name of this must be same as name for def contextElement!!!
        "others_wooden_window_page":others_wooden_window_catalog,
    }
    return render(request, 'others/others_wooden_window.html', context)

def others_autogate_system(request):
    others_autogate_system_catalog = OthersCatalog.objects.filter(type__type="Autogate System").order_by("my_id")
    context = {
        "others_autogate_system":page(others_autogate_system_catalog), #name of this must be same as name for def contextElement!!!
        "others_autogate_system_page":others_autogate_system_catalog,
    }
    return render(request, 'others/others_autogate_system.html', context)

def others_hardware_misc(request):
    others_hardware_misc_catalog = OthersCatalog.objects.filter(type__type="Hardware Misc").order_by("my_id")
    context = {
        "others_hardware_misc":page(others_hardware_misc_catalog), #name of this must be same as name for def contextElement!!!
        "others_hardware_misc_page":others_hardware_misc_catalog,
    }
    return render(request, 'others/others_hardware_misc.html', context)