from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.others, name='others'),
    url(r'^staircase$', views.others_staircase, name='others_staircase'),
    url(r'^wooden-window$', views.others_wooden_window, name='others_wooden_window'),
    url(r'^autogate-system$', views.others_autogate_system, name='others_autogate_system'),
    url(r'^hardware-misc$', views.others_hardware_misc, name='others_hardware_misc'),
]