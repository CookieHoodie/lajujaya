from django.contrib import admin
from .models import OthersType, OthersCatalog, Carosel

# Register your models here.

class OthersCatalogAdmin(admin.ModelAdmin):
    list_display = ('type','my_id','name', 'image', 'description')
    list_filter = ['type','name']

class CaroselAdmin(admin.ModelAdmin):
    list_display = ('name','page','image')

admin.site.register(OthersType)
admin.site.register(OthersCatalog, OthersCatalogAdmin)
admin.site.register(Carosel, CaroselAdmin)