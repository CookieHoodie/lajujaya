# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OthersCatalog',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('my_id', models.IntegerField(null=True)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='others/')),
                ('description', models.TextField(max_length=500, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='OthersType',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('type', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='otherscatalog',
            name='type',
            field=models.ForeignKey(to='others.OthersType'),
        ),
    ]
