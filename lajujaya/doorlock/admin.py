from django.contrib import admin
from .models import DoorlockType, DoorlockCatalog, DoorlockCategory

# Register your models here.

class DoorlockCatalogAdmin(admin.ModelAdmin):
    list_display = ('category','type','my_id','name','description')
    list_filter = ['category','type','name']

class DoorlockTypeAdmin(admin.ModelAdmin):
    ordering = ['type']

class DoorlockCategoryAdmin(admin.ModelAdmin):
    ordering = ['category']

admin.site.register(DoorlockType, DoorlockTypeAdmin)
admin.site.register(DoorlockCategory, DoorlockCategoryAdmin)
admin.site.register(DoorlockCatalog, DoorlockCatalogAdmin)
