from django.db import models
import os
# Create your models here.

def path_and_rename(instance, filename):
    upload_to = 'doorlock/'
    ext = filename.split('.')[-1]
    # get filename
    if instance.name:
        filename = '{}.{}'.format(instance.name, ext)
    else:
        pass
    # return the whole path to the file
    return os.path.join(upload_to, filename)

class DoorlockType(models.Model):
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.type

class DoorlockCategory(models.Model):
    category = models.CharField(max_length=50)

    def __str__(self):
        return self.category

class DoorlockCatalog(models.Model):
    category = models.ForeignKey(DoorlockCategory)
    type = models.ForeignKey(DoorlockType)
    my_id = models.IntegerField(null=True)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=path_and_rename)
    description = models.TextField(max_length=500, blank=True)

    def __str__(self):
        return self.name
