from django.shortcuts import render
from .models import DoorlockCatalog
# my own function
from myfunction import page
# Create your views here.

def doorlock(request):
    return render(request, "doorlock/doorlock.html", {})

# ----------------------------------- ST GUCHI DOOR LOCK --------------------------------------
def st_guchi(request):
    context = {}
    return render(request, 'doorlock/st_guchi.html', context)

def st_guchi_accessories(request):
    st_guchi_accessories_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Accessories').order_by("my_id")
    context = {
        "st_guchi_accessories":page(st_guchi_accessories_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_accessories_page":st_guchi_accessories_catalog,
    }
    return render(request, 'doorlock/st_guchi_accessories.html', context)

def st_guchi_cylindrical_lever(request):
    st_guchi_cylindrical_lever_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Cylindrical Lever').order_by("my_id")
    context = {
        "st_guchi_cylindrical_lever":page(st_guchi_cylindrical_lever_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_cylindrical_lever_page":st_guchi_cylindrical_lever_catalog,
    }
    return render(request, 'doorlock/st_guchi_cylindrical_lever.html', context)

def st_guchi_cylindrical_lock(request):
    st_guchi_cylindrical_lock_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Cylindrical Lock').order_by("my_id")
    context = {
        "st_guchi_cylindrical_lock":page(st_guchi_cylindrical_lock_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_cylindrical_lock_page":st_guchi_cylindrical_lock_catalog,
    }
    return render(request, 'doorlock/st_guchi_cylindrical_lock.html', context)

def st_guchi_deadbolt_lock(request):
    st_guchi_deadbolt_lock_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Deadbolt Lock').order_by("my_id")
    context = {
        "st_guchi_deadbolt_lock":page(st_guchi_deadbolt_lock_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_deadbolt_lock_page":st_guchi_deadbolt_lock_catalog,
    }
    return render(request, 'doorlock/st_guchi_deadbolt_lock.html', context)

def st_guchi_door_closer(request):
    st_guchi_door_closer_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Door Closer').order_by("my_id")
    context = {
        "st_guchi_door_closer":page(st_guchi_door_closer_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_door_closer_page":st_guchi_door_closer_catalog,
    }
    return render(request, 'doorlock/st_guchi_door_closer.html', context)

def st_guchi_handle_set(request):
    st_guchi_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Handle Set').order_by("my_id")
    context = {
        "st_guchi_handle_set":page(st_guchi_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_handle_set_page":st_guchi_handle_set_catalog,
    }
    return render(request, 'doorlock/st_guchi_handle_set.html', context)

def st_guchi_lever_handle(request):
    st_guchi_lever_handle_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Lever Handle').order_by("my_id")
    context = {
        "st_guchi_lever_handle":page(st_guchi_lever_handle_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_lever_handle_page":st_guchi_lever_handle_catalog,
    }
    return render(request, 'doorlock/st_guchi_lever_handle.html', context)

def st_guchi_mortise_handle_set(request):
    st_guchi_mortise_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Mortise Handle Set').order_by("my_id")
    context = {
        "st_guchi_mortise_handle_set":page(st_guchi_mortise_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_mortise_handle_set_page":st_guchi_mortise_handle_set_catalog,
    }
    return render(request, 'doorlock/st_guchi_mortise_handle_set.html', context)

def st_guchi_mortise_lock(request):
    st_guchi_mortise_lock_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Mortise Lock').order_by("my_id")
    context = {
        "st_guchi_mortise_lock":page(st_guchi_mortise_lock_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_mortise_lock_page":st_guchi_mortise_lock_catalog,
    }
    return render(request, 'doorlock/st_guchi_mortise_lock.html', context)

def st_guchi_mortise_hook_lock(request):
    st_guchi_mortise_hook_lock_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Mortise Lock & Hook Lock').order_by("my_id")
    context = {
        "st_guchi_mortise_hook_lock":page(st_guchi_mortise_hook_lock_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_mortise_hook_lock_page":st_guchi_mortise_hook_lock_catalog,
    }
    return render(request, 'doorlock/st_guchi_mortise_hook_lock.html', context)

def st_guchi_pad_lock(request):
    st_guchi_pad_lock_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Pad Lock').order_by("my_id")
    context = {
        "st_guchi_pad_lock":page(st_guchi_pad_lock_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_pad_lock_page":st_guchi_pad_lock_catalog,
    }
    return render(request, 'doorlock/st_guchi_pad_lock.html', context)

def st_guchi_profile_cylinder(request):
    st_guchi_profile_cylinder_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Profile Cylinder').order_by("my_id")
    context = {
        "st_guchi_profile_cylinder":page(st_guchi_profile_cylinder_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_profile_cylinder_page":st_guchi_profile_cylinder_catalog,
    }
    return render(request, 'doorlock/st_guchi_profile_cylinder.html', context)

def st_guchi_pull_handle(request):
    st_guchi_pull_handle_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Pull Handle').order_by("my_id")
    context = {
        "st_guchi_pull_handle":page(st_guchi_pull_handle_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_pull_handle_page":st_guchi_pull_handle_catalog,
    }
    return render(request, 'doorlock/st_guchi_pull_handle.html', context)

def st_guchi_tubular_lever(request):
    st_guchi_tubular_lever_catalog = DoorlockCatalog.objects.filter(category__category="ST Guchi Door Lock", type__type='Tubular Lever').order_by("my_id")
    context = {
        "st_guchi_tubular_lever":page(st_guchi_tubular_lever_catalog), #name of this must be same as name for def contextElement!!!
        "st_guchi_tubular_lever_page":st_guchi_tubular_lever_catalog,
    }
    return render(request, 'doorlock/st_guchi_tubular_lever.html', context)

# ------------------------------- IKAROS -------------------------------------
def ikaros(request):
    context = {}
    return render(request, 'doorlock/ikaros.html', context)

def ikaros_cylindrical_lever(request):
    ikaros_cylindrical_lever_catalog = DoorlockCatalog.objects.filter(category__category="Ikaros Door Lock", type__type='Cylindrical Lever').order_by("my_id")
    context = {
        "ikaros_cylindrical_lever":page(ikaros_cylindrical_lever_catalog), #name of this must be same as name for def contextElement!!!
        "ikaros_cylindrical_lever_page":ikaros_cylindrical_lever_catalog,
    }
    return render(request, 'doorlock/ikaros_cylindrical_lever.html', context)

def ikaros_pad_lock(request):
    ikaros_pad_lock_catalog = DoorlockCatalog.objects.filter(category__category="Ikaros Door Lock", type__type='Pad Lock').order_by("my_id")
    context = {
        "ikaros_pad_lock":page(ikaros_pad_lock_catalog), #name of this must be same as name for def contextElement!!!
        "ikaros_pad_lock_page":ikaros_pad_lock_catalog,
    }
    return render(request, 'doorlock/ikaros_pad_lock.html', context)

# --------------------------------DIGITAL DOOR LOCK----------------------------------
def digital_doorlock(request):
    context = {}
    return render(request, 'doorlock/digital_doorlock.html', context)

def digital_doorlock_samsung(request):
    digital_doorlock_samsung_catalog = DoorlockCatalog.objects.filter(category__category="Digital Door Lock", type__type='Samsung').order_by("my_id")
    context = {
        "digital_doorlock_samsung":page(digital_doorlock_samsung_catalog), #name of this must be same as name for def contextElement!!!
        "digital_doorlock_samsung_page":digital_doorlock_samsung_catalog,
    }
    return render(request, 'doorlock/digital_doorlock_samsung.html', context)

def digital_doorlock_gateman(request):
    digital_doorlock_gateman_catalog = DoorlockCatalog.objects.filter(category__category="Digital Door Lock", type__type='Gateman').order_by("my_id")
    context = {
        "digital_doorlock_gateman":page(digital_doorlock_gateman_catalog), #name of this must be same as name for def contextElement!!!
        "digital_doorlock_gateman_page":digital_doorlock_gateman_catalog,
    }
    return render(request, 'doorlock/digital_doorlock_gateman.html', context)

# ---------------------------------HAFELE-------------------------------------------
def hafele(request):
    context = {}
    return render(request, 'doorlock/hafele.html', context)

def hafele_cylindrical_lever_handle_set(request):
    hafele_cylindrical_lever_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="Hafele Door Lock", type__type='Cylindrical Lever Handle Set').order_by("my_id")
    context = {
        "hafele_cylindrical_lever_handle_set":page(hafele_cylindrical_lever_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "hafele_cylindrical_lever_handle_set_page":hafele_cylindrical_lever_handle_set_catalog,
    }
    return render(request, 'doorlock/hafele_cylindrical_lever_handle_set.html', context)

def hafele_lever_handle_set(request):
    hafele_lever_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="Hafele Door Lock", type__type='Lever Handle Set').order_by("my_id")
    context = {
        "hafele_lever_handle_set":page(hafele_lever_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "hafele_lever_handle_set_page":hafele_lever_handle_set_catalog,
    }
    return render(request, 'doorlock/hafele_lever_handle_set.html', context)

def hafele_tubular_lever_handle_set(request):
    hafele_tubular_lever_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="Hafele Door Lock", type__type='Tubular Lever Handle Set').order_by("my_id")
    context = {
        "hafele_tubular_lever_handle_set":page(hafele_tubular_lever_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "hafele_tubular_lever_handle_set_page":hafele_tubular_lever_handle_set_catalog,
    }
    return render(request, 'doorlock/hafele_tubular_lever_handle_set.html', context)

# ------------------------------------------VIVA---------------------------------------------
def viva(request):
    context = {}
    return render(request, 'doorlock/viva.html', context)

def viva_cylindrical_knob_set(request):
    viva_cylindrical_knob_set_catalog = DoorlockCatalog.objects.filter(category__category="Viva Door Lock", type__type='Cylindrical Knob Set').order_by("my_id")
    context = {
        "viva_cylindrical_knob_set":page(viva_cylindrical_knob_set_catalog), #name of this must be same as name for def contextElement!!!
        "viva_cylindrical_knob_set_page":viva_cylindrical_knob_set_catalog,
    }
    return render(request, 'doorlock/viva_cylindrical_knob_set.html', context)

def viva_hinge(request):
    viva_hinge_catalog = DoorlockCatalog.objects.filter(category__category="Viva Door Lock", type__type='Hinge').order_by("my_id")
    context = {
        "viva_hinge":page(viva_hinge_catalog), #name of this must be same as name for def contextElement!!!
        "viva_hinge_page":viva_hinge_catalog,
    }
    return render(request, 'doorlock/viva_hinge.html', context)

def viva_hook_lock(request):
    viva_hook_lock_catalog = DoorlockCatalog.objects.filter(category__category="Viva Door Lock", type__type='Hook Lock').order_by("my_id")
    context = {
        "viva_hook_lock":page(viva_hook_lock_catalog), #name of this must be same as name for def contextElement!!!
        "viva_hook_lock_page":viva_hook_lock_catalog,
    }
    return render(request, 'doorlock/viva_hook_lock.html', context)

def viva_tubular_lockset(request):
    viva_tubular_lockset_catalog = DoorlockCatalog.objects.filter(category__category="Viva Door Lock", type__type='Tubular Lockset').order_by("my_id")
    context = {
        "viva_tubular_lockset":page(viva_tubular_lockset_catalog), #name of this must be same as name for def contextElement!!!
        "viva_tubular_lockset_page":viva_tubular_lockset_catalog,
    }
    return render(request, 'doorlock/viva_tubular_lockset.html', context)

# --------------------------------------------GRESET-----------------------------------------------
def greset(request):
    context = {}
    return render(request, 'doorlock/greset.html', context)

def greset_cylindrical_lock(request):
    greset_cylindrical_lock_catalog = DoorlockCatalog.objects.filter(category__category="Greset Door Lock", type__type='Cylindrical Lock').order_by("my_id")
    context = {
        "greset_cylindrical_lock":page(greset_cylindrical_lock_catalog), #name of this must be same as name for def contextElement!!!
        "greset_cylindrical_lock_page":greset_cylindrical_lock_catalog,
    }
    return render(request, 'doorlock/greset_cylindrical_lock.html', context)

def greset_deadbolt_lock(request):
    greset_deadbolt_lock_catalog = DoorlockCatalog.objects.filter(category__category="Greset Door Lock", type__type='Deadbolt Lock').order_by("my_id")
    context = {
        "greset_deadbolt_lock":page(greset_deadbolt_lock_catalog), #name of this must be same as name for def contextElement!!!
        "greset_deadbolt_lock_page":greset_deadbolt_lock_catalog,
    }
    return render(request, 'doorlock/greset_deadbolt_lock.html', context)

def greset_handle_set(request):
    greset_handle_set_catalog = DoorlockCatalog.objects.filter(category__category="Greset Door Lock", type__type='Handle Set').order_by("my_id")
    context = {
        "greset_handle_set":page(greset_handle_set_catalog), #name of this must be same as name for def contextElement!!!
        "greset_handle_set_page":greset_handle_set_catalog,
    }
    return render(request, 'doorlock/greset_handle_set.html', context)

def greset_tubular_lever(request):
    greset_tubular_lever_catalog = DoorlockCatalog.objects.filter(category__category="Greset Door Lock", type__type='Tubular Lever').order_by("my_id")
    context = {
        "greset_tubular_lever":page(greset_tubular_lever_catalog), #name of this must be same as name for def contextElement!!!
        "greset_tubular_lever_page":greset_tubular_lever_catalog,
    }
    return render(request, 'doorlock/greset_tubular_lever.html', context)
