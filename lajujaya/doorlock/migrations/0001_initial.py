# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DoorlockCatalog',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('my_id', models.IntegerField(null=True)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='doorlock/')),
                ('description', models.TextField(max_length=500, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DoorlockCategory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('category', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='DoorlockType',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('type', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='doorlockcatalog',
            name='category',
            field=models.ForeignKey(to='doorlock.DoorlockCategory'),
        ),
        migrations.AddField(
            model_name='doorlockcatalog',
            name='type',
            field=models.ForeignKey(to='doorlock.DoorlockType'),
        ),
    ]
