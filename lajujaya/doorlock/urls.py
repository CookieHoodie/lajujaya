from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.doorlock, name='doorlock'),
    # -----------------------------ST GUCHI------------------------------------
    url(r'^st-guchi/$', views.st_guchi, name="st_guchi"),
    url(r'^st-guchi/accessories$', views.st_guchi_accessories, name='st_guchi_accessories'),
    url(r'^st-guchi/cylindrical-lever$', views.st_guchi_cylindrical_lever, name="st_guchi_cylindrical_lever"),
    url(r'^st-guchi/cylindrical-lock$', views.st_guchi_cylindrical_lock, name="st_guchi_cylindrical_lock"),
    url(r'^st-guchi/deadbolt-lock$', views.st_guchi_deadbolt_lock, name="st_guchi_deadbolt_lock"),
    url(r'^st-guchi/door-closer$', views.st_guchi_door_closer, name="st_guchi_door_closer"),
    url(r'^st-guchi/handle-set$', views.st_guchi_handle_set, name="st_guchi_handle_set"),
    url(r'^st-guchi/lever-handle$', views.st_guchi_lever_handle, name="st_guchi_lever_handle"),
    url(r'^st-guchi/mortise-handle-set$', views.st_guchi_mortise_handle_set, name="st_guchi_mortise_handle_set"),
    url(r'^st-guchi/mortise-lock-and-hook-lock$', views.st_guchi_mortise_hook_lock, name="st_guchi_mortise_hook_lock"),
    url(r'^st-guchi/mortise-lock$', views.st_guchi_mortise_lock, name="st_guchi_mortise_lock"),
    url(r'^st-guchi/pad-lock$', views.st_guchi_pad_lock, name="st_guchi_pad_lock"),
    url(r'^st-guchi/profile-cylinder$', views.st_guchi_profile_cylinder, name="st_guchi_profile_cylinder"),
    url(r'^st-guchi/pull-handle$', views.st_guchi_pull_handle, name="st_guchi_pull_handle"),
    url(r'^st-guchi/tubular-lever$', views.st_guchi_tubular_lever, name="st_guchi_tubular_lever"),
    # --------------------------------IKAROS------------------------------------
    url(r'^ikaros/$', views.ikaros, name='ikaros'),
    url(r'^ikaros/cylindrical-lever$', views.ikaros_cylindrical_lever, name='ikaros_cylindrical_lever'),
    url(r'^ikaros/pad-lock$', views.ikaros_pad_lock, name='ikaros_pad_lock'),
    # -----------------------------DIGITAL DOOR LOCK--------------------------------
    url(r'^digital-door-lock/$', views.digital_doorlock, name='digital_doorlock'),
    url(r'^digital-door-lock/samsung$', views.digital_doorlock_samsung, name='digital_doorlock_samsung'),
    url(r'^digital-door-lock/gateman$', views.digital_doorlock_gateman, name='digital_doorlock_gateman'),
    # ---------------------------------HAFELE----------------------------------------
    url(r'^hafele/$', views.hafele, name='hafele'),
    url(r'^hafele/cylindrical-lever-handle-set$', views.hafele_cylindrical_lever_handle_set, name='hafele_cylindrical_lever_handle_set'),
    url(r'^hafele/lever-handle-set$', views.hafele_lever_handle_set, name='hafele_lever_handle_set'),
    url(r'^hafele/tubular-lever-handle-set$', views.hafele_tubular_lever_handle_set, name='hafele_tubular_lever_handle_set'),
    # -------------------------------------VIVA------------------------------------------
    url(r'^viva/$', views.viva, name='viva'),
    url(r'^viva/cylindrical-knob-set$', views.viva_cylindrical_knob_set, name='viva_cylindrical_knob_set'),
    url(r'^viva/hinge$', views.viva_hinge, name='viva_hinge'),
    url(r'^viva/hook-lock$', views.viva_hook_lock, name='viva_hook_lock'),
    url(r'^viva/tubular-lockset$', views.viva_tubular_lockset, name='viva_tubular_lockset'),
    # ----------------------------------------GRESET---------------------------------------------
    url(r'^greset/$', views.greset, name='greset'),
    url(r'^greset/cylindrical-lock$', views.greset_cylindrical_lock, name='greset_cylindrical_lock'),
    url(r'^greset/deadbolt-lock$', views.greset_deadbolt_lock, name='greset_deadbolt_lock'),
    url(r'^greset/handle-set$', views.greset_handle_set, name='greset_handle_set'),
    url(r'^greset/tubular-lever$', views.greset_tubular_lever, name='greset_tubular_lever'),
]