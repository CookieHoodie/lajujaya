from django.shortcuts import render
from .models import AccessoriesCatalog
from myfunction import page
# Create your views here.

def kitchen_bathroom_accessories(request):
    context = {}
    return render(request, 'kitchen_bathroom_accessories/k_b_accessories.html', context)

def sink(request):
    sink_catalog = AccessoriesCatalog.objects.filter(type__type='Sink').order_by('my_id')
    context = {
        'sink':page(sink_catalog),
        'sink_page':sink_catalog,
    }
    return render(request, 'kitchen_bathroom_accessories/sink.html', context)

def bathroom_accessories(request):
    bathroom_accessories_catalog = AccessoriesCatalog.objects.filter(type__type='Bathroom Accessories').order_by('my_id')
    context = {
        'bathroom_accessories':page(bathroom_accessories_catalog),
        'bathroom_accessories_page':bathroom_accessories_catalog,
    }
    return render(request, 'kitchen_bathroom_accessories/bathroom_accessories.html', context)

def kitchen_accessories(request):
    kitchen_accessories_catalog = AccessoriesCatalog.objects.filter(type__type='Kitchen Accessories').order_by('my_id')
    context = {
        'kitchen_accessories':page(kitchen_accessories_catalog),
        'kitchen_accessories_page':kitchen_accessories_catalog,
    }
    return render(request, 'kitchen_bathroom_accessories/kitchen_accessories.html', context)

def water_tap(request):
    water_tap_catalog = AccessoriesCatalog.objects.filter(type__type='Water Tap').order_by('my_id')
    context = {
        'water_tap':page(water_tap_catalog),
        'water_tap_page':water_tap_catalog,
    }
    return render(request, 'kitchen_bathroom_accessories/water_tap.html', context)