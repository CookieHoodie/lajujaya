from django.contrib import admin
from .models import AccessoriesType, AccessoriesCatalog

# Register your models here.

class AccessoriesCatalogAdmin(admin.ModelAdmin):
    list_display = ('type','my_id','name', 'image', 'description')
    list_filter = ['type','name']

admin.site.register(AccessoriesType)
admin.site.register(AccessoriesCatalog, AccessoriesCatalogAdmin)
