from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.kitchen_bathroom_accessories, name='kitchen_bathroom_accessories'),
    url(r'^sink/$', views.sink, name="sink"),
    url(r'^kitchen-accessories/$', views.kitchen_accessories, name="kitchen_accessories"),
    url(r'^bathroom-accessories/$', views.bathroom_accessories, name="bathroom_accessories"),
    url(r'^water-tap/$', views.water_tap, name="water_tap"),
]