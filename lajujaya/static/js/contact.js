$(document).ready(function(){
    var cache = new Selector_Cache();

    cache('#contact-form').submit(function() { // catch the form's submit event
        cache('button.dis').addClass('disabled');
        cache('button.dis').attr('disabled','disabled');
        $.ajax({ // create an AJAX call...
            data: $(this).serialize(), // get the form data
            type: $(this).attr('method'), // GET or POST
            url: '/enquiry/contact', // the file to call
            success: function(response) { // on success..
                cache('#contact-form').html(response);  // get the form in the /enquiry into the modal
                if($('#contact-form >  #notice').hasClass('alert') == false){ //cant use cache becuase it's changing
                    cache('button.dis').removeClass('disabled');
                    cache('button.dis')[0].removeAttribute('disabled');
                }
            },
            error: function(e, x, r) { // on error..
                cache('#notice').attr('class','alert alert-danger'); // update the DIV
                cache('#notice').text('There is some error within the system, please contact us via 07-3596775 / 016-7185081 or email us at lajujayated@gmail.com ! Sorry for your inconvinience !')
            }
        });
        return false;
    });
});
