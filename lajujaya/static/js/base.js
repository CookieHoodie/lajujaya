function Selector_Cache() {
	var elementCache = {};

	var get_from_cache = function( selector, $ctxt, reset ) {

		if ( 'boolean' === typeof $ctxt ) { reset = $ctxt; }
		var cacheKey = $ctxt ? $ctxt.selector + ' ' + selector : selector;

		if ( undefined === elementCache[ cacheKey ] || reset ) {
			elementCache[ cacheKey ] = $ctxt ? $ctxt.find( selector ) : jQuery( selector );
		}

		return elementCache[ cacheKey ];
	};

	get_from_cache.elementCache = elementCache;
	return get_from_cache;
}

$(document).ready(function(){
	//------------------------------------//
	//Scroll To Top//
	//------------------------------------//
	var cache = new Selector_Cache();
	// Check to see if the window is top if not then display button
	function scrollToTop(){
		if (cache(window).scrollTop() > 300) {
			cache('.scrollToTop').fadeIn();
		} else {
			cache('.scrollToTop').fadeOut();
		}
	}
    var scrollTimer = null;
    cache(window).scroll(function () {
        if (scrollTimer) {
            clearTimeout(scrollTimer);   // clear any previous pending timer
        }
        scrollTimer = setTimeout(scrollToTop, 150);   // set new timer
    });

	//Click event to scroll to top
	cache('.scrollToTop').click(function(){
		cache('html, body').animate({scrollTop : 0},800);
		return false;
	});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=196218974051569";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
});