$(document).ready(function(){

    var cache = new Selector_Cache();
    function navbarCss() {
        cache('.navbar-fixed-top').addClass('navbar-fixed-top-css');
        cache('.navbar-default .navbar-brand').addClass('navbar-default-brand-css');
        cache('.navbar-default .navbar-nav>li>.dropdown-toggle').addClass('dropdown-toggle-css');
        cache('.navbar-default .navbar-nav>li>a').addClass('a-css');
        cache('.manderin').addClass('manderin-css');
    }

    function changingNavbar(){
        if (cache(window).scrollTop() == 0) {
            navbarCss()
        } else {
            if(cache('.navbar-fixed-top').hasClass('navbar-fixed-top-css')&&
               cache('.navbar-default .navbar-brand').hasClass('navbar-default-brand-css')&&
               cache('.navbar-default .navbar-nav>li>.dropdown-toggle').hasClass('dropdown-toggle-css')&&
               cache('.navbar-default .navbar-nav>li>a').hasClass('a-css')&&
               cache('.manderin').hasClass('manderin-css'))
            {
               cache('.navbar-fixed-top').removeClass('navbar-fixed-top-css');
               cache('.navbar-default .navbar-brand').removeClass('navbar-default-brand-css');
               cache('.navbar-default .navbar-nav>li>.dropdown-toggle').removeClass('dropdown-toggle-css');
               cache('.navbar-default .navbar-nav>li>a').removeClass('a-css');
               setTimeout(function() {
                   cache('.manderin').removeClass("manderin-css");
               }, 500);
            }
        }
    }

    if(cache(window).scrollTop() == 0){
        navbarCss();
    }

    var scrollTimer = null;
    cache(window).scroll(function () {
        if (scrollTimer) {
            clearTimeout(scrollTimer);   // clear any previous pending timer
        }
        scrollTimer = setTimeout(changingNavbar, 100);   // set new timer
    });
});