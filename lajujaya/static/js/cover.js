$(document).ready(function(){
    var cache = new Selector_Cache();
    var myCenter = new google.maps.LatLng(1.573851,103.766931);
    function initialize() {
      var mapProp = {
        center: myCenter,
        zoom:17,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      };
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter
      });

      marker.setMap(map);
    }
    google.maps.event.addDomListener(window, 'load', initialize);

  cache("#owl-demo").owlCarousel({
    singleItem : true,
      slideSpeed : 500,
      autoPlay : 3000
  });

    wow = new WOW({
        offset:60
     });
    wow.init();
});