$(document).ready(function(){

    var cache = new Selector_Cache();
    function jqUpdateSize(){
        // Get the dimensions of the viewport
        var win_height = $(window).height();
        if((win_height) > cache('#nav').outerHeight(true)+40) {
        //see if the windows height is > than #nav height, if it is, affix, if not,dont so that #nav bottom can be seen
            cache('#nav').css('position',''); //everytime reload or resize window, initialize the position of #nav so that wont be affected by else statement below
            cache('#nav').affix({  // sidebar affix
                offset: {
                    top: cache('.jumbotron').outerHeight(true),
                    bottom: cache('footer').outerHeight(true) + 40
                }
            });
            cache('#nav-top').affix({  // page bar affix
                offset: {
                    top: cache('.jumbotron').outerHeight(true) - (cache('.navbar').outerHeight() - 2),
                    bottom: cache('footer').outerHeight(true) + 40
                }
            });
        }else {
            cache('#nav').css('position','static'); // if window height < #nav height, make it static but not affix
        }
        // make the width of #nav same when toggle between affix, affix-top, affix-bottom
        var nav_width = $('#sidebar-nav').width(); // sidebar column's width
        var navtop_width = $('#content-col').width(); // main content column's width
        cache('#nav').css('width', nav_width);
        cache('#nav-top').css('width', navtop_width);
    }
    jqUpdateSize();   // When the page first loads
    cache(window).resize(jqUpdateSize);     // When the browser changes size

    var pathname = window.location.pathname; // url of current page
	cache('#nav > li > a[href="'+pathname+'"]').parent().addClass('active');
    var active_href = cache('#nav > li > a[href="'+pathname+'"]');
    cache("#nav-top > li > h4").text((active_href).text());

    // change the page href base on the #nav active href's id
    cache('#nav-top > li > a').each(function(){
        var data_href = ('#' + active_href.data('href'));
        $(this).attr('href', function(_,val){
            return val.replace(/\D+/g, data_href);
        });
        $(this).attr('aria-controls', function(_,val){
            return val.replace(/\D+/g, data_href);
        });
    });

    // hide all the page and then see the number of tab-content available to show them
    var num = 2;
    cache(".tab-content > div").each(function(){
        if($("[id^='+active_href+']",this)){ // active_href var come from above
            cache("#nav-top > li:lt('"+num+"')").css('display','inline-block');
            num = num + 1;
        }
    });

    // get the data on the clicked image to fill in the modal and the form
    cache("#modal").on('show.bs.modal', function(event){
        var trigger = $(event.relatedTarget);
        var name = trigger.data("name");
        var image = trigger.data("image");
        var description = trigger.data("description");
        var type = trigger.data("type");
        $(this).find("h4").text(name);
        $(this).find(".img-thumbnail").attr({"src":image, "alt":name});
        $(this).find(".modal-body > #modal_description > ul").html(description);
        $(this).find("#id_product").val(type + "  |  " + name);
    });
    cache('#modal-form').submit(function() { // catch the form's submit event
        cache('button.dis').addClass('disabled');
        cache('button.dis').attr('disabled','disabled');
        $.ajax({ // create an AJAX call...
            data: $(this).serialize(), // get the form data
            type: $(this).attr('method'), // GET or POST
            url: '/enquiry/', // the file to call
            success: function(response) { // on success..
                cache('#modal-form').html(response);  // get the form in the /enquiry into the modal
                if($('#modal-form >  #notice').hasClass('alert-success') == false){ //cant use cache becuase it's changing
                    cache('button.dis').removeClass('disabled');
                    cache('button.dis')[0].removeAttribute('disabled');
                }
            },
            error: function(e, x, r) { // on error..
                cache('#notice').attr('class','alert alert-danger'); // update the DIV
                cache('#notice').text('There is some error within the system, please contact us via 07-3596775 / 016-7185081 or email us at lajujayated@gmail.com ! Sorry for your inconvinience !')
            }
        });
        return false;
    });
    // load form in /enquiry page when loading the product's pages
    cache("#modal-form").load('/enquiry/');
    // reload the form when the modal is closed
    cache('#modal').on('hidden.bs.modal', function(){
        cache("#modal-form").load('/enquiry/');
        cache('button.dis').removeClass('disabled');
        cache('button.dis')[0].removeAttribute('disabled');
    });
    //$("#nav a").on("shown.bs.tab", function(event){
    //    window.active_href = $(event.target).attr("href");
    //    var active_text = $(event.target).text();
    //    $("#nav-top > li > a").each(function(index){
    //        $(this).attr("href", active_href + (index+1));      //maintain later!!!!!!!!!!!!!!!!!!!
    //        if($(this).attr("href") == (active_href+1)){        //make a function return #nav li.active a .href
    //            $(this).attr("href", active_href);          //n replace active_href with that function so that
    //        }                                               //this script runs when the page load, not after click
    //    });
    //    $("#nav-top > li > h4").text(active_text);
    //});
    //$("#nav a").click(function(){ //make the page1 active everytime when #nav a change
    //    if($(this).attr("href") != $("#nav > li.active > a").attr("href")){
    //        $("#nav-top > li").removeClass("active");
    //        $("#nav-top > li.a").addClass("active");
    //    }
    //});
    //var num = 2;
    //$(".tab-content > div").each(function(){
    //    if($(this).attr("id").match(/SWD\d/)){
    //        $("#nav-top > li:lt('"+num+"')").show();
    //        $("#nav-top > li").eq(num).show();
    //        $("#nav-top > li:gt('"+num+"')").hide();
    //        num = num + 1
    //    }
    //    else{
    //        num = 1;
    //        $("#nav-top > li:lt('"+num+"')").show();
    //        $("#nav-top > li:gt('"+num+"')").hide();
    //    }
    //});

    // make content scroll back to top when the tabs are clicked
    cache('#nav-top a').click(function(e){
        cache('html, body').animate({
            scrollTop: cache('#content-col').offset().top - 60
        }, 'slow');
        e.preventDefault();
    });
});

